<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Articles',
	'Simply News'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Simply News');

$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . articles;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/Plugin.xml');


t3lib_extMgm::addLLrefForTCAdescr('tx_simplynews_domain_model_article', 'EXT:simply_news/Resources/Private/Language/locallang_csh_tx_simplynews_domain_model_article.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_simplynews_domain_model_article');
$TCA['tx_simplynews_domain_model_article'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY date DESC',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
			'fe_group' => 'fe_group',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Article.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_simplynews_domain_model_article.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_simplynews_domain_model_category', 'EXT:simply_news/Resources/Private/Language/locallang_csh_tx_simplynews_domain_model_category.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_simplynews_domain_model_category');
$TCA['tx_simplynews_domain_model_category'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_category',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Category.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_simplynews_domain_model_category.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_simplynews_domain_model_media', 'EXT:simply_news/Resources/Private/Language/locallang_csh_tx_simplynews_domain_model_media.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_simplynews_domain_model_media');
$TCA['tx_simplynews_domain_model_media'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Media.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_simplynews_domain_model_media.gif'
	),
);

?>