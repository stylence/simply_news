<?php
	if (!defined('TYPO3_MODE')) {
		die ('Access denied.');
	}

	Tx_Extbase_Utility_Extension::configurePlugin(
		$_EXTKEY,
		'Articles',
		array(
			'Article' => 'delete, edit, update, list, latest, new, create, show, archiveMenu, search',
			'Category' => 'delete, edit, update, list, new, create, show',
			'Media' => 'delete, edit, update, list, new, create, show',

		),
		// non-cacheable actions
		array(
			'Article' => 'delete, update, create, search',
			'Category' => 'delete, update, create',
			'Media' => 'delete, update, create',

		)
	);

?>
