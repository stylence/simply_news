<?php
	if (!defined('TYPO3_MODE')) {
		die ('Access denied.');
	}

	$TCA['tx_simplynews_domain_model_article'] = array(
		'ctrl' => $TCA['tx_simplynews_domain_model_article']['ctrl'],
		'interface' => array(
			'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, fe_group, date, teaser, text, categories, media',
		),
		'types' => array(
			'1' => array('showitem' => 'title;;1;, teaser, text;;;richtext:rte_transform[mode=ts_css|imgpath=uploads/tx_simplynews/rte/],
		--div--;Relations, categories, media,
		--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden, fe_group, starttime, endtime'),
		),
		'palettes' => array(
			'1' => array('showitem' => 'date', 'doNotCollapse' => 1),
		),
		'columns' => array(
			'fe_group' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.fe_group',
				'config' => array(
					'type' => 'select',
					'size' => 5,
					'maxitems' => 20,
					'items' => array(
						array(
							'LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login',
							-1,
						),
						array(
							'LLL:EXT:lang/locallang_general.xml:LGL.any_login',
							-2,
						),
						array(
							'LLL:EXT:lang/locallang_general.xml:LGL.usergroups',
							'--div--',
						),
					),
					'exclusiveKeys' => '-1,-2',
					'foreign_table' => 'fe_groups',
					'foreign_table_where' => 'ORDER BY fe_groups.title',
				),
			),
			'sys_language_uid' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
				'config' => array(
					'type' => 'select',
					'foreign_table' => 'sys_language',
					'foreign_table_where' => 'ORDER BY sys_language.title',
					'items' => array(
						array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
						array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
					),
				),
			),
			'l10n_parent' => array(
				'displayCond' => 'FIELD:sys_language_uid:>:0',
				'exclude' => 1,
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
				'config' => array(
					'type' => 'select',
					'items' => array(
						array('', 0),
					),
					'foreign_table' => 'tx_simplynews_domain_model_article',
					'foreign_table_where' => 'AND tx_simplynews_domain_model_article.pid=###CURRENT_PID### AND tx_simplynews_domain_model_article.sys_language_uid IN (-1,0)',
				),
			),
			'l10n_diffsource' => array(
				'config' => array(
					'type' => 'passthrough',
				),
			),
			't3ver_label' => array(
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
				'config' => array(
					'type' => 'input',
					'size' => 30,
					'max' => 255,
				)
			),
			'hidden' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
				'config' => array(
					'type' => 'check',
				),
			),
			'starttime' => array(
				'exclude' => 1,
				'l10n_mode' => 'mergeIfNotBlank',
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
				'config' => array(
					'type' => 'input',
					'size' => 13,
					'max' => 20,
					'eval' => 'datetime',
					'checkbox' => 0,
					'default' => 0,
					'range' => array(
						'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
					),
				),
			),
			'endtime' => array(
				'exclude' => 1,
				'l10n_mode' => 'mergeIfNotBlank',
				'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
				'config' => array(
					'type' => 'input',
					'size' => 13,
					'max' => 20,
					'eval' => 'datetime',
					'checkbox' => 0,
					'default' => 0,
					'range' => array(
						'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
					),
				),
			),
			'title' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article.title',
				'config' => array(
					'type' => 'input',
					'size' => 40,
					'eval' => 'trim,required,unique'
				),
			),
			'date' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article.date',
				'config' => array(
					'type' => 'input',
					'size' => 12,
					'max' => 20,
					'eval' => 'datetime,required',
					'checkbox' => 1,
					'default' => time()
				),
			),
			'teaser' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article.teaser',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 5,
					'eval' => 'trim'
				),
			),
			'text' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article.text',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim,required'
				),
			),
			'categories' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article.categories',
				'config' => array(
					'type' => 'select',
					'foreign_table' => 'tx_simplynews_domain_model_category',
					'foreign_table_where' => ' AND tx_simplynews_domain_model_category.sys_language_uid IN (-1,0)',
					'minitems' => 0,
					'maxitems' => 1,
					'suppress_icons' => 1,
					'renderMode' => 'tree',
					'treeConfig' => array(
						'parentField' => 'parent',
						'appearance' => array(
							'expandAll' => TRUE,
							'showHeader' => TRUE,
						),
					),
				),
			),
			'media' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_article.media',
				'config' => array(
					'type' => 'select',
					'foreign_table' => 'tx_simplynews_domain_model_media',
					'maxitems' => 9999,
					'size' => 10,
					'multiple' => 1,
					'wizards' => array(
						'_PADDING' => 2,
						'_VERTICAL' => 2,
						'add' => Array(
							'type' => 'script',
							'title' => 'Create new',
							'icon' => 'add.gif',
							'params' => array(
								'table' => 'tx_simplynews_domain_model_media',
								'pid' => '###CURRENT_PID###',
								'setValue' => 'prepend'
							),
							'script' => 'wizard_add.php',
						),
					),
				),
			),
		),
	);
?>
