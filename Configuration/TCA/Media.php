<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_simplynews_domain_model_media']['ctrl']['requestUpdate'] = 'media_type';
$TCA['tx_simplynews_domain_model_media'] = array(
	'ctrl' => $TCA['tx_simplynews_domain_model_media']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, media_type, reference, image_position',
	),
	'types' => array(
		'1' => array('showitem' => 'media_type, title, description, reference, image, image_position,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages',
					      -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value',
					      0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_simplynews_domain_model_media',
				'foreign_table_where' => 'AND tx_simplynews_domain_model_media.pid=###CURRENT_PID### AND tx_simplynews_domain_model_media.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'media_type' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media.media_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('Select the type of media', 0),
					array('Image file', 1),
					array('Youtube reference', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => 'required',
				'default' => 1,
			),
		),
		'reference' => array(
			'exclude' => 0,
			'displayCond' => 'FIELD:media_type:=:2',
			'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media.reference',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'image' => array(
			'exclude' => 1,
			'displayCond' => 'FIELD:media_type:=:1',
			'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media.image',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'uploadfolder' => 'uploads/tx_simplynews/media/',
				'show_thumbs' => 1,
				'size' => 5,
				'maxitems' => 9999,
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
				'disallowed' => '',
			),
		),
		'image_position' => array(
			'exclude' => 0,
			'displayCond' => 'FIELD:media_type:=:1',
			'label' => 'LLL:EXT:simply_news/Resources/Private/Language/locallang_db.xml:tx_simplynews_domain_model_media.image_position',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('Right', 0),
					array('Left', 1),
					array('Centered', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'article' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'category' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
?>