<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_SimplyNews_Domain_Model_Media.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Simply News
 *
 * @author Henjo Hoeksma <hphoeksma@stylence.nl>
 */
class Tx_SimplyNews_Domain_Model_MediaTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_SimplyNews_Domain_Model_Media
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_SimplyNews_Domain_Model_Media();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() { 
		$this->fixture->setTitle('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTitle()
		);
	}
	
	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription() { 
		$this->fixture->setDescription('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getDescription()
		);
	}
	
	/**
	 * @test
	 */
	public function getMediaTypeReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getMediaType()
		);
	}

	/**
	 * @test
	 */
	public function setMediaTypeForIntegerSetsMediaType() { 
		$this->fixture->setMediaType(12);

		$this->assertSame(
			12,
			$this->fixture->getMediaType()
		);
	}
	
	/**
	 * @test
	 */
	public function getReferenceReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setReferenceForStringSetsReference() { 
		$this->fixture->setReference('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getReference()
		);
	}
	
	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setImageForStringSetsImage() { 
		$this->fixture->setImage('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getImage()
		);
	}
	
	/**
	 * @test
	 */
	public function getImagePositionReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getImagePosition()
		);
	}

	/**
	 * @test
	 */
	public function setImagePositionForIntegerSetsImagePosition() { 
		$this->fixture->setImagePosition(12);

		$this->assertSame(
			12,
			$this->fixture->getImagePosition()
		);
	}
	
}
?>