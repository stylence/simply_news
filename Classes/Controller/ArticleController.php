<?php

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2011 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

	/**
	 *
	 *
	 * @package simply_news
	 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
	 *
	 */
	class Tx_SimplyNews_Controller_ArticleController extends Tx_Extbase_MVC_Controller_ActionController {

		/**
		 * articleRepository
		 *
		 * @var Tx_SimplyNews_Domain_Repository_ArticleRepository
		 */
		protected $articleRepository;

		/**
		 * injectArticleRepository
		 *
		 * @param Tx_SimplyNews_Domain_Repository_ArticleRepository $articleRepository
		 * @return void
		 */
		public function injectArticleRepository(Tx_SimplyNews_Domain_Repository_ArticleRepository $articleRepository) {
			$this->articleRepository = $articleRepository;
		}

		/**
		 * action delete
		 *
		 * @param $article
		 * @return void
		 */
		public function deleteAction(Tx_SimplyNews_Domain_Model_Article $article) {
			$this->articleRepository->remove($article);
			$this->flashMessageContainer->add('Your Article was removed.');
			$this->redirect('list');
		}

		/**
		 * action edit
		 *
		 * @param $article
		 * @return void
		 */
		public function editAction(Tx_SimplyNews_Domain_Model_Article $article) {
			$this->view->assign('article', $article);
		}

		/**
		 * action update
		 *
		 * @param $article
		 * @return void
		 */
		public function updateAction(Tx_SimplyNews_Domain_Model_Article $article) {
			$this->articleRepository->update($article);
			$this->flashMessageContainer->add('Your Article was updated.');
			$this->redirect('list');
		}

		/**
		 * action list
		 *
		 * @return void
		 */
		public function listAction() {
			$arguments = $this->request->getArguments();
			if ($arguments['category']||$this->settings['list']['category']) {
				$selectedCategory = $arguments['category'] ? $arguments['category'] : $this->settings['list']['category'];
				$categoryRepository = $this->objectManager->get(Tx_SimplyNews_Domain_Repository_CategoryRepository);
				$category = $categoryRepository->findByUid($selectedCategory);
				$articles = $this->articleRepository->findByCategories($category);
				$this->view->assign('category', $category);
			} elseif ($arguments['year'] && $arguments['month']) {
				$articles = $this->articleRepository->findByYearAndMonth($arguments['year'], $arguments['month']);
				$dateUsedInArchive = new DateTime($arguments['month'] . '/01/' . $arguments['year']);
				$this->view->assign('dateUsedInArchive', $dateUsedInArchive);
			} elseif ($arguments['year']) {
				$articles = $this->articleRepository->findByYear($arguments['year']);
				$this->view->assign('dateUsedInArchive', $arguments['year']);
				$this->view->assign('isYearArchive', TRUE);
			} else {
				if ($this->settings['list']['limit']) {
					$articles = $this->articleRepository->findAll($this->settings['list']['limit']);
				} else {
					$articles = $this->articleRepository->findAll();
				}
			}
			$this->view->assign('articles', $articles);
		}

		/**
		 * action latest
		 *
		 * @return void
		 */
		public function latestAction() {
			$arguments = $this->request->getArguments();
			if ($arguments['category']||$this->settings['list']['category']) {
				$selectedCategory = $arguments['category'] ? $arguments['category'] : $this->settings['list']['category'];
				$categoryRepository = $this->objectManager->get(Tx_SimplyNews_Domain_Repository_CategoryRepository);
				$category = $categoryRepository->findByUid($selectedCategory);
				if ($this->settings['list']['limit']) {
					$articles = $this->articleRepository->findByCategories($category, $this->settings['list']['limit']);
				} else {
					$articles = $this->articleRepository->findByCategories($category);
				}
				$this->view->assign('category', $category);
			} elseif ($this->settings['list']['limit']) {
				$articles = $this->articleRepository->findAll($this->settings['list']['limit']);
			} else {
				$articles = $this->articleRepository->findAll();
			}
			$this->view->assign('articles', $articles);
		}

		/**
		 * action new
		 *
		 * @param $newArticle
		 * @dontvalidate $newArticle
		 * @return void
		 */
		public function newAction(Tx_SimplyNews_Domain_Model_Article $newArticle = NULL) {
			$this->view->assign('newArticle', $newArticle);
		}

		/**
		 * action create
		 *
		 * @param $newArticle
		 * @return void
		 */
		public function createAction(Tx_SimplyNews_Domain_Model_Article $newArticle) {
			$this->articleRepository->add($newArticle);
			$this->flashMessageContainer->add('Your new Article was created.');
			$this->redirect('list');
		}

		/**
		 * action show
		 *
		 * @param $article
		 * @return void
		 */
		public function showAction(Tx_SimplyNews_Domain_Model_Article $article = NULL) {
			if ($article === NULL && $this->settings['show']['fallBackToLatestArticle']) {
				$arguments = $this->request->getArguments();
				if ($arguments['category']||$this->settings['list']['category']) {
					$selectedCategory = $arguments['category'] ? $arguments['category'] : $this->settings['list']['category'];
					$categoryRepository = $this->objectManager->get(Tx_SimplyNews_Domain_Repository_CategoryRepository);
					$category = $categoryRepository->findByUid($selectedCategory);
					$article = $this->articleRepository->findByCategories($category, 1)->getFirst();
				} else {
					$article = $this->articleRepository->findAll($limit = 1)->getFirst();
				}
			}
			if ($article) {
				// Set Page title
				$GLOBALS['TSFE']->page['title'] = $article->getTitle();
				// Set Title if IS is used
				$GLOBALS['TSFE']->indexedDocTitle = $article->getTitle();
				$this->view->assign('article', $article);
			}
		}

		/**
		 * Show the archive menu
		 *
		 * @return void
		 */
		public function archiveMenuAction() {
			$years = $this->articleRepository->findAllDates();
			$this->view->assign('years', $years);
		}

		/**
		 * @return void
		 */
		public function searchAction() {
			$arguments = $this->request->getArguments();
			if ($arguments['term']) {
				$articles = $this->articleRepository->findByTerm($arguments['term']);
				$this->view->assign('term', $arguments['term']);
				$this->view->assign('articles', $articles);
			} else {
				$this->flashMessageContainer->add(Tx_Extbase_Utility_Localization::translate('message.search.noTermSpecified', $this->extensionName));
			}
		}
	}

?>
