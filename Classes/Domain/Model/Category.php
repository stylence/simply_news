<?php

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2011 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

	/**
	 *
	 *
	 * @package simply_news
	 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
	 *
	 */
	class Tx_SimplyNews_Domain_Model_Category extends Tx_Extbase_DomainObject_AbstractEntity {

		/**
		 * Title
		 *
		 * @var string
		 * @validate NotEmpty
		 */
		protected $title;

		/**
		 * Description
		 *
		 * @var string
		 */
		protected $description;

		/**
		 * Media
		 *
		 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Media>
		 */
		protected $media;

		/**
		 * Parent Category
		 *
		 * @var Tx_SimplyNews_Domain_Model_Category
		 */
		protected $parent;

		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() {
			// Do not remove the next line: It would break the functionality
			$this->initStorageObjects();
		}

		/**
		 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
		 *
		 * @return void
		 */
		protected function initStorageObjects() {
			/**
			 * Do not modify this method!
			 * It will be rewritten on each save in the extension builder
			 * You may modify the constructor of this class instead
			 */
			$this->media = new Tx_Extbase_Persistence_ObjectStorage();
		}

		/**
		 * Returns the title
		 *
		 * @return string $title
		 */
		public function getTitle() {
			return $this->title;
		}

		/**
		 * Sets the title
		 *
		 * @param string $title
		 * @return void
		 */
		public function setTitle($title) {
			$this->title = $title;
		}

		/**
		 * Returns the description
		 *
		 * @return string $description
		 */
		public function getDescription() {
			return $this->description;
		}

		/**
		 * Sets the description
		 *
		 * @param string $description
		 * @return void
		 */
		public function setDescription($description) {
			$this->description = $description;
		}

		/**
		 * Adds a Media
		 *
		 * @param Tx_SimplyNews_Domain_Model_Media $medium
		 * @return void
		 */
		public function addMedium(Tx_SimplyNews_Domain_Model_Media $medium) {
			$this->media->attach($medium);
		}

		/**
		 * Removes a Media
		 *
		 * @param Tx_SimplyNews_Domain_Model_Media $mediumToRemove The Media to be removed
		 * @return void
		 */
		public function removeMedium(Tx_SimplyNews_Domain_Model_Media $mediumToRemove) {
			$this->media->detach($mediumToRemove);
		}

		/**
		 * Returns the media
		 *
		 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Media> $media
		 */
		public function getMedia() {
			return $this->media;
		}

		/**
		 * Sets the media
		 *
		 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Media> $media
		 * @return void
		 */
		public function setMedia(Tx_Extbase_Persistence_ObjectStorage $media) {
			$this->media = $media;
		}

		/**
		 * Returns the parent
		 *
		 * @return Tx_SimplyNews_Domain_Model_Category $parent
		 */
		public function getParent() {
			return $this->parent;
		}

		/**
		 * Sets the parent
		 *
		 * @param Tx_SimplyNews_Domain_Model_Category $parent
		 * @return void
		 */
		public function setParent(Tx_SimplyNews_Domain_Model_Category $parent) {
			$this->parent = $parent;
		}

	}

?>