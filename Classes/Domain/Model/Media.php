<?php

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2011 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

	/**
	 *
	 *
	 * @package simply_news
	 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
	 *
	 */
	class Tx_SimplyNews_Domain_Model_Media extends Tx_Extbase_DomainObject_AbstractEntity {

		/**
		 * Title
		 *
		 * @var string
		 */
		protected $title;

		/**
		 * Description
		 *
		 * @var string
		 */
		protected $description;

		/**
		 * Media Type
		 *
		 * @var integer
		 * @validate NotEmpty
		 */
		protected $mediaType;

		/**
		 * Reference
		 *
		 * @var string
		 */
		protected $reference;

		/**
		 * Position
		 *
		 * @var integer
		 */
		protected $imagePosition;

		/**
		 * Position
		 *
		 * @var string
		 */
		protected $image;

		/**
		 * Returns the mediaType
		 *
		 * @return integer $mediaType
		 */
		public function getMediaType() {
			return $this->mediaType;
		}

		/**
		 * Sets the mediaType
		 *
		 * @param integer $mediaType
		 * @return void
		 */
		public function setMediaType($mediaType) {
			$this->mediaType = $mediaType;
		}

		/**
		 * Returns the reference
		 *
		 * @return string $reference
		 */
		public function getReference() {
			return $this->reference;
		}

		/**
		 * Sets the reference
		 *
		 * @param string $reference
		 * @return void
		 */
		public function setReference($reference) {
			$this->reference = $reference;
		}

		/**
		 * Returns the imagePosition
		 *
		 * @return integer $imagePosition
		 */
		public function getImagePosition() {
			return $this->imagePosition;
		}

		/**
		 * Sets the imagePosition
		 *
		 * @param integer $imagePosition
		 * @return void
		 */
		public function setImagePosition($imagePosition) {
			$this->imagePosition = $imagePosition;
		}

		/**
		 * Returns the title
		 *
		 * @return string $title
		 */
		public function getTitle() {
			return $this->title;
		}

		/**
		 * Sets the title
		 *
		 * @param string $title
		 * @return void
		 */
		public function setTitle($title) {
			$this->title = $title;
		}

		/**
		 * Returns the description
		 *
		 * @return string $description
		 */
		public function getDescription() {
			return $this->description;
		}

		/**
		 * Sets the description
		 *
		 * @param string $description
		 * @return void
		 */
		public function setDescription($description) {
			$this->description = $description;
		}

		/**
		 * Returns the image
		 *
		 * @return string $image
		 */
		public function getImage() {
			return $this->image;
		}

		/**
		 * Sets the image
		 *
		 * @param string $image
		 * @return void
		 */
		public function setImage($image) {
			$this->image = $image;
		}

	}

?>