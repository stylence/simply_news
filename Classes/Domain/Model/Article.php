<?php

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2011 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

	/**
	 *
	 *
	 * @package simply_news
	 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
	 *
	 */
	class Tx_SimplyNews_Domain_Model_Article extends Tx_Extbase_DomainObject_AbstractEntity {

		/**
		 * Title
		 *
		 * @var string
		 * @validate NotEmpty
		 */
		protected $title;

		/**
		 * Date
		 *
		 * @var DateTime
		 * @validate NotEmpty
		 */
		protected $date;

		/**
		 * Teaser
		 *
		 * @var string
		 */
		protected $teaser;

		/**
		 * Content
		 *
		 * @var string
		 * @validate NotEmpty
		 */
		protected $text;

		/**
		 * categories
		 *
		 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Category>
		 */
		protected $categories;

		/**
		 * Media
		 *
		 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Media>
		 */
		protected $media;

		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() {
			$this->initStorageObjects();
		}

		/**
		 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
		 *
		 * @return void
		 */
		protected function initStorageObjects() {
			/**
			 * Do not modify this method!
			 * It will be rewritten on each save in the extension builder
			 * You may modify the constructor of this class instead
			 */
			$this->categories = new Tx_Extbase_Persistence_ObjectStorage();

			$this->media = new Tx_Extbase_Persistence_ObjectStorage();
		}

		/**
		 * Returns the title
		 *
		 * @return string $title
		 */
		public function getTitle() {
			return $this->title;
		}

		/**
		 * Sets the title
		 *
		 * @param string $title
		 * @return void
		 */
		public function setTitle($title) {
			$this->title = $title;
		}

		/**
		 * Returns the date
		 *
		 * @return DateTime $date
		 */
		public function getDate() {
			return $this->date;
		}

		/**
		 * Sets the date
		 *
		 * @param DateTime $date
		 * @return void
		 */
		public function setDate($date) {
			$this->date = $date;
		}

		/**
		 * Returns the text
		 *
		 * @return string $text
		 */
		public function getText() {
			return $this->text;
		}

		/**
		 * Sets the text
		 *
		 * @param string $text
		 * @return void
		 */
		public function setText($text) {
			$this->text = $text;
		}

		/**
		 * Adds a Category
		 *
		 * @param Tx_SimplyNews_Domain_Model_Category $category
		 * @return void
		 */
		public function addCategory(Tx_SimplyNews_Domain_Model_Category $category) {
			$this->categories->attach($category);
		}

		/**
		 * Removes a Category
		 *
		 * @param Tx_SimplyNews_Domain_Model_Category $categoryToRemove The Category to be removed
		 * @return void
		 */
		public function removeCategory(Tx_SimplyNews_Domain_Model_Category $categoryToRemove) {
			$this->categories->detach($categoryToRemove);
		}

		/**
		 * Returns the categories
		 *
		 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Category> $categories
		 */
		public function getCategories() {
			return $this->categories;
		}

		/**
		 * Sets the categories
		 *
		 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Category> $categories
		 * @return void
		 */
		public function setCategories(Tx_Extbase_Persistence_ObjectStorage $categories) {
			$this->categories = $categories;
		}

		/**
		 * Adds a Media
		 *
		 * @param Tx_SimplyNews_Domain_Model_Media $medium
		 * @return void
		 */
		public function addMedium(Tx_SimplyNews_Domain_Model_Media $medium) {
			$this->media->attach($medium);
		}

		/**
		 * Removes a Media
		 *
		 * @param Tx_SimplyNews_Domain_Model_Media $mediumToRemove The Media to be removed
		 * @return void
		 */
		public function removeMedium(Tx_SimplyNews_Domain_Model_Media $mediumToRemove) {
			$this->media->detach($mediumToRemove);
		}

		/**
		 * Returns the media
		 *
		 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Media> $media
		 */
		public function getMedia() {
			return $this->media;
		}

		/**
		 * Sets the media
		 *
		 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyNews_Domain_Model_Media> $media
		 * @return void
		 */
		public function setMedia(Tx_Extbase_Persistence_ObjectStorage $media) {
			$this->media = $media;
		}

		/**
		 * @param string $teaser
		 */
		public function setTeaser($teaser) {
			$this->teaser = $teaser;
		}

		/**
		 * @return string
		 */
		public function getTeaser() {
			return $this->teaser;
		}

	}

?>