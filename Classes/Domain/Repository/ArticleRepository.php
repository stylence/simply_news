<?php

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2011 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

	/**
	 *
	 *
	 * @package simply_news
	 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
	 *
	 */
	class Tx_SimplyNews_Domain_Repository_ArticleRepository extends Tx_Extbase_Persistence_Repository {

		protected $defaultOrderings = array('date' => Tx_Extbase_Persistence_QueryInterface::ORDER_DESCENDING);

		/**
		 * @param integer $limit
		 * @return void
		 */
		public function findAll($limit = NULL) {
			$query = $this->createQuery();
			if ($limit) {
				$query->setLimit((integer)$limit);
			}
			return $query->execute();
		}

		/**
		 * Finds most recent articles
		 *
		 * @return array of years containing array of months
		 */
		public function findAllDates() {
			$query = $this->createQuery();
			$res = $query->execute();
			$yearArray = array();
			foreach ($res as $row) {
				$date = $row->getDate();
				$year = $date->format('Y');
				$month = $date->format('m');
				$day = $date->format('d');
				if (!isset($yearArray[$year])) {
					$yearArray[$year] = array('count' => 0);
				}
				if (!isset($yearArray[$year]['months'][$month])) {
					$yearArray[$year]['months'][$month] = array('count' => 0);
				}
				if (!isset($yearArray[$year]['months'][$month]['days'][$day])) {
					$yearArray[$year]['months'][$month]['days'][$day] = array('count' => 0);
				}
				$yearArray[$year]['count']++;
				$yearArray[$year]['months'][$month]['count']++;
				$yearArray[$year]['months'][$month]['days'][$day]['count']++;
				$yearArray[$year]['value'] = intval($year);
				$yearArray[$year]['datetime'] = $date;
				$yearArray[$year]['months'][$month]['value'] = intval($month);
				$yearArray[$year]['months'][$month]['datetime'] = $date;
				$yearArray[$year]['months'][$month]['days'][$day]['value'] = intval($day);
				$yearArray[$year]['months'][$month]['days'][$day]['datetime'] = $date;
				$yearArray[$year]['year'] = $year;
			}
			return $yearArray;
		}

		/**
		 *
		 * @param string $year
		 * @param string $month
		 *
		 * @return void
		 */
		public function findByYearAndMonth($year, $month) {
			$query = $this->createQuery();
			$timeStart = new DateTime($month . '/01/' . $year);
			$timeEnd = new DateTime($month . '/31/' . $year);
			$results = $query->matching(
				$query->logicalAnd($query->greaterThanOrEqual('date', $timeStart),
					$query->lessThanOrEqual('date', $timeEnd))
			)->execute();

			return $results;
		}

		/**
		 * @param string $year
		 * @return void
		 */
		public function findByYear($year) {
			$query = $this->createQuery();
			$timeStart = new DateTime('01/01/' . $year);
			$timeEnd = new DateTime('12/31/' . $year);
			$results = $query->matching(
				$query->logicalAnd($query->greaterThanOrEqual('date', $timeStart),
					$query->lessThanOrEqual('date', $timeEnd))
			)->execute();

			return $results;
		}

		/**
		 *
		 * @return array $categories
		 */
		public function findDistinctCategories() {
			$query = $this->createQuery();
			$articles = $query->execute();
			$categories = array();
			foreach ($articles as $article) {
				foreach ($article->getCategories() as $category) {
					$categories[$category->getUid()]['uid'] = $category->getUid();
					$categories[$category->getUid()]['title'] = $category->getTitle();
					$categories[$category->getUid()]['count']++;
				}
			}
			return $categories;
		}

		/**
		 * @param string $term
		 * @return array|Tx_Extbase_Persistence_QueryResultInterface
		 */
		public function findByTerm($term) {
			$query = $this->createQuery();
			$constraints = $query->logicalOr(
				$query->like('title', '%' . $term . '%'),
				$query->like('teaser', '%' . $term . '%'),
				$query->like('text', '%' . $term . '%')
			);

			return $query->matching($constraints)->execute();
		}

		/**
		 * @param Tx_SimplyNews_Domain_Model_Category $category
		 * @param int $limit
		 * @return array|Tx_Extbase_Persistence_QueryResultInterface
		 */
		public function findByCategories(Tx_SimplyNews_Domain_Model_Category $category, $limit = NULL) {
			$query = $this->createQuery();
			$constraints = $query->logicalAnd(
				$query->contains('categories', $category)
			);
			if ($limit) {
				return $query->matching($constraints)->setLimit((int) $limit)->execute();
			}

			return $query->matching($constraints)->execute();
		}
	}

?>
